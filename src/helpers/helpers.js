
function calcXe(val, carbCount, etalon=12){
    let xeCount = ((carbCount / 100) * val) / etalon
    return xeCount.toFixed(1)
}

export default calcXe;
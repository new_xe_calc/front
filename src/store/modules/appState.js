import { stat } from "fs";

const state = {
  xeItems: [],
  xeCount: 0,
  backLink: ''
};

const mutations = {

    setBackLink(state, path){
      state.backLink = path
    },

    addToXeItem(state, item) {
      state.xeCount += parseFloat(item.xeCount)
      state.xeItems.push(item)
    },

    deleteXeItem(state, index) {
      let xeCount = parseFloat(state.xeItems[index].xeCount)
      state.xeCount -= xeCount
      state.xeItems.splice(index, 1)
    },

    deleteAllItems(state){
      state.xeCount = 0
      state.xeItems = [] 
    }
};

export default {
  namespaced: true,
  state,
  mutations
}

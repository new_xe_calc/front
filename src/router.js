import Vue from 'vue'
import Router from 'vue-router'
import ResultList from './views/ResultList.vue'
import SearchAndCalc from './views/SearchAndCalc.vue'
import ProductCalc from './views/ProductCalc.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Список',
      meta: {'title': 'Список продуктов'},
      component: ResultList
    },
    {
      path: '/add_product',
      name: 'Добавление продукта',
      meta: {'title': 'Добавление продукта'},
      component: SearchAndCalc
    },
    {
      path: '/calc_product/:id',
      name: 'Продукт',
      meta: {'title': 'Продукт'},
      component: ProductCalc
    },

  ]
})

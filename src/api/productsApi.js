import api_instance from './server'

const prefix = '/products/'

const productsApi = {

    getProdList(name){
      let query_str = '?search=' + name
      return api_instance.get(prefix+query_str)
    },

    getProductDetail(id){
      return api_instance.get(prefix + id + "/")
    },

    getProductPortions(id){
      return api_instance.get('/portions/?product=' + id)
    }
}

export default productsApi;
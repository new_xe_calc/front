import axios from 'axios'

let config;
config = {
  baseURL: process.env.VUE_APP_SERVER_DOMAIN + '/api/',
  timeout: 1000
}

const api_instance = axios.create(config);

export default api_instance;
import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import 'vuetify/src/stylus/app.styl'

Vue.use(Vuetify, {
  iconfont: 'md',
  theme: {
    primary: '#8bc34a',
    secondary: '#009688',
    accent: '#00bcd4',
    error: '#f44336',
    warning: '#ff9800',
    info: '#2196f3',
    success: '#3f51b5'
  }
})
